<?php

namespace App\Helpers;

use App\Exceptions\ValidationError;
use Illuminate\Validation\ValidationException;

class ResponseHelper
{
      public static function makeSuccess($data = [], $message = null)
      {
          $response = [
              'success' => true,
              'data' => $data
          ];

          if($message){
              $response['message'] = $message;
          }

          return response()->json($response);

      }

      public static function makeError($errors = [], $message = null)
      {
          $response = [
              'success' => false,
              'errors' => $errors
          ];

          if($message){
              $response['message'] = $message;
          }

          return response()->json($response);
      }

//    public static function makeExceptionError(\Exception $exception, $message = null)
//    {
//        //throw $exception;
//
//        $response = [
//            'success' => false,
//        ];
//
//        if ($exception instanceof ValidationException) {
//            $errors             = $exception->validator->errors();
//            $response['errors'] = $errors;
//            if ($message) {
//                $response['message'] = $message;
//            }
//        } elseif ($exception instanceof ValidationError) {
//            $errors             = $exception->getErrors();
//            $response['errors'] = $errors;
//            if ($message) {
//                $response['message'] = $message;
//            }
//        } elseif (property_exists($exception, 'showToUser') && $exception->showToUser) {
//            $response['errors']  = [ 'error' => $exception->getMessage() ];
//            $response['message'] = ( ! is_null($message) ) ?: $exception->getMessage();
//        } else {
//            $response['errors']  = [ 'error' => 'Неизвестная ошибка сервера' ];
//            $response['message'] = ( ! is_null($message) ) ?: 'Неизвестная ошибка сервера';
//        }
//
//        return response()->json($response);
//    }
}