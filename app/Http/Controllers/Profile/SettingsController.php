<?php

namespace App\Http\Controllers\Profile;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Helpers\ResponseHelper;

use Illuminate\Support\Facades\Validator;

use App\User;
use Auth;

class SettingsController extends Controller
{
    public function getUserInfo(Request $request)
    {
        return ResponseHelper::makeSuccess(Auth::user());
    }
    protected  function validateAvatar(array $file){
        $validator = Validator::make($file, [
           'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        return $validator;
    }
    protected  function validateProfile(array $data){
        $validator =  Validator::make($data, [
            'login' => 'required|string|max:255',
            'email' => 'required|string|email|max:255'
        ]);

        return $validator;
    }

    public function updateAvatar(Request $request)
    {
        //Валидация
        $validate_result = $this->validateAvatar( $request->all() );

        if( $validate_result->fails() ){
            return ResponseHelper::makeError($validate_result->errors());
        }

        //Название и расширение
        $file = $request->file('avatar');
        $ext  = $file->guessClientExtension();
        //Генерим название для файла
        $file_name = date('Y-m-d_H:i:s') . ".{$ext}";
        //Получаем user_id
        $user_id = Auth::user()->id;

        //Директория сохранения
        $save_path = "avatars/{$user_id}/{$file_name}";

        //Размещаем файл
        $file->move(public_path('/storage/uploads/') . "avatars/{$user_id}", $file_name);

        //Записываем данные
        $user = User::find($user_id);
        $user->avatar = $save_path;
        $user->save();

        return ResponseHelper::makeSuccess([
            'avatar_url' => $save_path
        ]);
    }
    public function updateProfile(Request $request){
        $validate_result = $this->validateProfile( $request->all() );

        if( $validate_result->fails() ){
            return ResponseHelper::makeError($validate_result->errors());
        }

        $user_id = Auth::user()->id;

        //Записываем данные
        $user = User::find($user_id);

        $user->login = $request->login;
        $user->email = $request->email;

        $user->save();

//        Auth::logout();

        return ResponseHelper::makeSuccess();

    }
    public function index()
    {
        return view('profile.settings');
    }
}
