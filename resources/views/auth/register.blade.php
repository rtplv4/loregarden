@extends('layouts.main')

@section('content')
    <div class="page-container container">
        <div class="form-block">
            <div class="form-block__header">
                <h2>Регистрация</h2>
            </div>
            <div class="form-block__body">
                <form id="registrationForm" class="form-horizontal" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('login') ? ' has-error' : '' }}">
                        <label for="login" class="col-md-4 control-label">Логин</label>

                        <div class="col-md-6">
                            <input id="login" type="text" class="form-control" name="login" value="{{ old('login') }}" required autofocus>

                            @if ($errors->has('login'))
                                <div class="form-errors__block">
                                    <span class="form-errors__row">{{ $errors->first('login') }}</span>
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">E-Mail адрес</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                                <div class="form-errors__block">
                                    <span class="form-errors__row">{{ $errors->first('email') }}</span>
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-4 control-label">Пароль</label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control" name="password" required>

                            @if ($errors->has('password'))
                                <div class="form-errors__block">
                                    <span class="form-errors__row">{{ $errors->first('password') }}</span>
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password-confirm" class="col-md-4 control-label">Повторите пароль</label>

                        <div class="col-md-6">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button class="btn btn-primary" type="submit">
                                Регистрация
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
