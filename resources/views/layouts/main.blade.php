<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ mix('css/main.css') }}" rel="stylesheet">

    {{--CSRF_token to window--}}
    <script>
      window.csrf_token = '{{ csrf_token() }}';
    </script>

</head>
<body>
    @include('layouts._partial.main.header')

    <div id="AppHolder">
        @yield('content')
    </div>

    @include('layouts._partial.main.footer')

    <script src="{{ mix('js/main.js') }}"></script>

    @stack('scripts')
</body>
</html>
