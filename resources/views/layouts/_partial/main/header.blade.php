<header class="app-header fixed-top">
    <nav class="app-nav navbar navbar-toggleable-md navbar-light bg-faded">
        <a class="navbar-brand" href="{{ url('/') }}">
            <img src="/images/logo.png" style="display: inline-block" width="28px" alt="логотип">
            <span style="display: inline-block; margin-left: 5px; vertical-align: middle;">{{ config('app.name', 'Laravel') }}</span>
        </a>
        <ul class="nav">
            @guest
                <li class="nav-item">
                    <a class="nav-link active" href="{{ route('login') }}">
                        <i class="fa fa-sign-in" aria-hidden="true"></i>
                        Войти
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">
                        <i class="fa fa-user-plus" aria-hidden="true"></i>
                        Регистрация
                    </a>
                </li>
            @else
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        <img class="nav-user-avatar" src="/storage/uploads/{{Auth::user()->avatar}}" alt="Аватар">
                        {{ Auth::user()->login }}
                    </a>

                    <div class="dropdown-menu">
                        <a href="{{ route('profile-settings') }}"
                           class="dropdown-item">
                            <i class="fa fa-cog" aria-hidden="true"></i>
                            Настройки
                        </a>
                        <a href="{{ route('logout') }}"
                           class="dropdown-item"
                           onclick="event.preventDefault(); document.getElementById('logoutForm').submit();">
                            <i class="fa fa-sign-out" aria-hidden="true"></i>
                            Выход
                        </a>
                    </div>

                    <form id="logoutForm" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            @endguest
        </ul>
    </nav>
</header>