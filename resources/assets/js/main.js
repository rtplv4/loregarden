require('./bootstrap');

import Vue from 'vue';

/** Главная - '/' **/
Vue.component('home', require('./views/Home/Home.vue'));

/** Профиль/Настройки - '/profile/settings' **/
Vue.component('profile-settings', require('./views/Profile/Settings/Settings.vue'));

const app = new Vue({
  el: '#AppHolder'
});
